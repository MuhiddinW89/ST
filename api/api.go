package api

import (
	_ "sarkorT/api/docs"
	"sarkorT/api/handler"
	"sarkorT/internal/service"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func SetUpApi(r *gin.Engine, service *service.Service) {
	handlerV1 := handler.NewHandlerV1(*service)

	usr := r.Group("/user")
	{
		usr.POST("/register", handlerV1.CreateUser)
		usr.POST("/auth", handlerV1.Login)
	}

	// r.POST("/users", handlerV1.CreateUser)
	// r.GET("/users/:id", handlerV1.GetFilmById)
	// r.GET("/users", handlerV1.GetFilmList)
	// r.PUT("/users/:id", handlerV1.UpdateFilm)
	// r.DELETE("/users/:id", handlerV1.DeleteFilm)

	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
}
