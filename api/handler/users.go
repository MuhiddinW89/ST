package handler

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"sarkorT/models"
	"sarkorT/pkg/helper"
	"strconv"

	"github.com/gin-gonic/gin"
)

// CreateUser godoc
// @ID create_user
// @Router /user/register [POST]
// @Summary Create User
// @Description Create User
// @Tags Users
// @Accept json
// @Produce json
// @Param user body models.CreateUser true "CreateUserRequestBody"
// @Success 201 {object} models.Users "GetUserBody"
// @Response 400 {object} string "Invalid Argument"
// @Failure 500 {object} string "Server Error"
func (h *HandlerV1) CreateUser(c *gin.Context) {
	var usr models.CreateUser

	login := c.PostForm("login")
	if login == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"Message": "Invalid login argument",
			"Data":    nil,
			"Error":   errors.New("bad request error here"),
		})
		return
	}

	password := c.PostForm("password")
	if password == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"Message": "Invalid password argument",
			"Data":    nil,
			"Error":   errors.New("bad request error here"),
		})
		return
	}

	name := c.PostForm("name")
	if name == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"Message": "Invalid name argument",
			"Data":    nil,
			"Error":   errors.New("bad request error here"),
		})
		return
	}

	age_str := c.PostForm("age")
	if name == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"Message": "Invalid age argument",
			"Data":    nil,
			"Error":   errors.New("bad request error here"),
		})
		return
	}

	age, err := strconv.Atoi(age_str)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"Message": "Invalid type of age",
			"Data":    nil,
			"Error":   err,
		})
		return
	}
	usr.Login = login
	usr.Password = password
	usr.Name = name
	usr.Age = age

	user_id, err := h.service.User.Create(c, &usr)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusCreated, gin.H{
		"id": user_id,
	})
}

// Login godoc
// @ID login
// @Router /user/auth [POST]
// @Summary Login
// @Description Login
// @Tags Users
// @Accept json
// @Produce json
// @Param user body models.LoginRequest true "LoginRequest"
// @Success 201 {object} models.Users "GetUserBody"
// @Response 400 {object} string "Invalid Argument"
// @Failure 500 {object} string "Server Error"
func (h *HandlerV1) Login(c *gin.Context) {
	var usr models.LoginUser

	if err := c.ShouldBindJSON(&usr); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error": "invalid data",
		})
		return
	}

	usr_id, err := h.service.User.Login(c, &usr)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	var accesspayload models.AccessPayload = models.AccessPayload{
		UserId: usr_id,
	}

	ap, err := json.Marshal(accesspayload)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	jwt_token, err := helper.GenerateJWT(string(ap))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.SetCookie("SESSTOKEN", fmt.Sprintf("Bearer %s", jwt_token), 1*24*60*60, "/", "", false, false)

	c.JSON(http.StatusCreated, gin.H{
		"message": "successfully logged in",
	})
}

// // GetByIdUsers godoc
// // @ID get_by_id_actor
// // @Router /actor/{id} [GET]
// // @Summary Get By Id Users
// // @Description Get By Id Users
// // @Tags Users
// // @Accept json
// // @Produce json
// // @Param id path string true "id"
// // @Success 200 {object} models.Users "GetUsersBody"
// // @Response 400 {object} string "Invalid Argument"
// // @Failure 500 {object} string "Server Error"
// func (h *HandlerV1) GetUsersById(c *gin.Context) {

// 	id := c.Param("id")

// 	resp, err := h.storage.Users().GetByPKey(
// 		context.Background(),
// 		&models.UsersPrimaryKey{UsersId: id},
// 	)

// 	if err != nil {
// 		log.Printf("error whiling GetByPKey: %v\n", err)
// 		c.JSON(http.StatusInternalServerError, errors.New("error whiling GetByPKey").Error())
// 		return
// 	}

// 	c.JSON(http.StatusOK, resp)
// }

// // GetListUsers godoc
// // @ID get_list_actor
// // @Router /actor [GET]
// // @Summary Get List Users
// // @Description Get List Users
// // @Tags Users
// // @Accept json
// // @Produce json
// // @Param offset query string false "offset"
// // @Param limit query string false "limit"
// // @Success 200 {object} models.GetListUsersResponse "GetUsersBody"
// // @Response 400 {object} string "Invalid Argument"
// // @Failure 500 {object} string "Server Error"
// func (h *HandlerV1) GetUsersList(c *gin.Context) {
// 	var (
// 		limit  int
// 		offset int
// 		err    error
// 	)

// 	limitStr := c.Query("limit")
// 	if limitStr != "" {
// 		limit, err = strconv.Atoi(limitStr)
// 		if err != nil {
// 			log.Printf("error whiling limit: %v\n", err)
// 			c.JSON(http.StatusBadRequest, err.Error())
// 			return
// 		}
// 	}

// 	offsetStr := c.Query("offset")
// 	if offsetStr != "" {
// 		offset, err = strconv.Atoi(offsetStr)
// 		if err != nil {
// 			log.Printf("error whiling limit: %v\n", err)
// 			c.JSON(http.StatusBadRequest, err.Error())
// 			return
// 		}
// 	}

// 	resp, err := h.storage.Users().GetList(
// 		context.Background(),
// 		&models.GetListUsersRequest{
// 			Limit:  int32(limit),
// 			Offset: int32(offset),
// 		},
// 	)

// 	if err != nil {
// 		log.Printf("error whiling get list: %v", err)
// 		c.JSON(http.StatusInternalServerError, errors.New("error whiling get list").Error())
// 		return
// 	}

// 	c.JSON(http.StatusOK, resp)
// }

// // UpdateUsers godoc
// // @ID update_actor
// // @Router /actor/{id} [PUT]
// // @Summary Update Users
// // @Description Update Users
// // @Tags Users
// // @Accept json
// // @Produce json
// // @Param id path string true "id"
// // @Param actor body models.UpdateUsers true "CreateUsersRequestBody"
// // @Success 200 {object} models.Users "GetactorsBody"
// // @Response 400 {object} string "Invalid Argument"
// // @Failure 500 {object} string "Server Error"
// func (h *HandlerV1) UpdateUsers(c *gin.Context) {

// 	var (
// 		actor models.UpdateUsers
// 	)

// 	id := c.Param("id")

// 	if id == "" {
// 		log.Printf("error whiling update: %v\n", errors.New("required actor id").Error())
// 		c.JSON(http.StatusBadRequest, errors.New("required actor id").Error())
// 		return
// 	}

// 	err := c.ShouldBindJSON(&actor)
// 	if err != nil {
// 		log.Printf("error whiling update: %v\n", err)
// 		c.JSON(http.StatusBadRequest, err.Error())
// 		return
// 	}

// 	rowsAffected, err := h.storage.Users().Update(
// 		context.Background(),
// 		id,
// 		&actor,
// 	)

// 	if err != nil {
// 		log.Printf("error whiling update: %v", err)
// 		c.JSON(http.StatusInternalServerError, errors.New("error whiling update").Error())
// 		return
// 	}

// 	if rowsAffected == 0 {
// 		log.Printf("error whiling update rows affected: %v", err)
// 		c.JSON(http.StatusInternalServerError, errors.New("error whiling update rows affected").Error())
// 		return
// 	}

// 	resp, err := h.storage.Users().GetByPKey(
// 		context.Background(),
// 		&models.UsersPrimarKey{Id: id},
// 	)

// 	if err != nil {
// 		log.Printf("error whiling GetByPKey: %v\n", err)
// 		c.JSON(http.StatusInternalServerError, errors.New("error whiling GetByPKey").Error())
// 		return
// 	}

// 	c.JSON(http.StatusOK, resp)
// }

// // DeleteByIdUsers godoc
// // @ID delete_by_id_actor
// // @Router /actor/{id} [DELETE]
// // @Summary Delete By Id Users
// // @Description Delete By Id Users
// // @Tags Users
// // @Accept json
// // @Produce json
// // @Param id path string true "id"
// // @Success 200 {object} models.Users "GetUsersBody"
// // @Response 400 {object} string "Invalid Argument"
// // @Failure 500 {object} string "Server Error"
// func (h *HandlerV1) DeleteUsers(c *gin.Context) {

// 	id := c.Param("id")
// 	if id == "" {
// 		log.Printf("error whiling update: %v\n", errors.New("required actor id").Error())
// 		c.JSON(http.StatusBadRequest, errors.New("required actor id").Error())
// 		return
// 	}

// 	err := h.storage.Users().Delete(
// 		context.Background(),
// 		&models.UsersPrimarKey{
// 			Id: id,
// 		},
// 	)

// 	if err != nil {
// 		log.Printf("error whiling delete: %v", err)
// 		c.JSON(http.StatusInternalServerError, errors.New("error whiling delete").Error())
// 		return
// 	}

// 	c.JSON(http.StatusNoContent, nil)
// }
