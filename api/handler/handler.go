package handler

import (
	"sarkorT/internal/service"
)

type HandlerV1 struct {
	service service.Service
}

func NewHandlerV1(service service.Service) *HandlerV1 {
	return &HandlerV1{
		service: service,
	}
}
