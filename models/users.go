package models

type User struct {
	Id       string `db:"id" json:"id" binding:"required"`
	Login    string `db:"login" json:"login" binding:"required"`
	Password string `db:"password" json:"password" binding:"required"`
	Name     string `db:"name" json:"name" binding:"required"`
	Age      int    `db:"age" json:"age" binding:"required"`
}

type CreateUser struct {
	Login    string `db:"login" json:"login" binding:"required"`
	Password string `db:"password" json:"password" binding:"required"`
	Name     string `db:"name" json:"name" binding:"required"`
	Age      int    `db:"age" json:"age" binding:"required"`
}

type LoginUser struct {
	Login    string `db:"login" json:"login" binding:"required"`
	Password string `db:"password" json:"password" binding:"required"`
}

type IdTracker struct {
	Id string `db:"id" json:"id" binding:"required"`
}

type AccessPayload struct {
	UserId string `db:"user_id" json:"user_id" binding:"required"`
}
