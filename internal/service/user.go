package service

import (
	"context"
	"sarkorT/internal/storage"
	"sarkorT/models"
)

type userService struct {
	userRepo storage.UserRepoI
}

func NewUserService(userRepo storage.UserRepoI) *userService {
	return &userService{
		userRepo: userRepo,
	}
}

func (us *userService) Create(ctx context.Context, req *models.CreateUser) (string, error) {
	return us.userRepo.Create(ctx, req)
}

func (us *userService) Login(ctx context.Context, req *models.LoginUser) (string, error) {
	return us.userRepo.Login(ctx, req)
}
