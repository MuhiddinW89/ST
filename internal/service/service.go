package service

import (
	"context"
	"sarkorT/internal/storage"
	"sarkorT/models"
)

type Service struct {
	User UserServiceI
}

func NewService(storage storage.StorageI) *Service {
	return &Service{
		User: NewUserService(storage.User()),
	}
}

type UserServiceI interface {
	Create(ctx context.Context, req *models.CreateUser) (string, error)
	Login(ctx context.Context, req *models.LoginUser) (string, error)
}
