package storage

import (
	"context"

	"sarkorT/models"
)

type StorageI interface {
	CloseDB()
	User() UserRepoI
}

type UserRepoI interface {
	Create(ctx context.Context, req *models.CreateUser) (string, error)
	Login(ctx context.Context, req *models.LoginUser) (string, error)
	// GetByPKey(ctx context.Context, req *models.UsersPrimaryKey) (*models.Users, error)
	// GetList(ctx context.Context, req *models.GetListUsersRequest) (*models.GetListUsersResponse, error)
	// Update(ctx context.Context, req *models.UpdateUsers) (int64, error)
	// Delete(ctx context.Context, req *models.UsersPrimaryKey) error
}
