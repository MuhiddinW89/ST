package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"

	"sarkorT/config"
	"sarkorT/internal/storage"
)

type Store struct {
	db   *pgxpool.Pool
	user *userRepo
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(fmt.Sprintf(
		"postgres://%s:%s@%s:%s/%s?sslmode=disable",
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDatabase,
	))
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections

	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	if err := pool.Ping(ctx); err != nil {
		return nil, err
	}

	return &Store{
		db:   pool,
		user: NewUserRepo(pool),
	}, err
}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (u *Store) User() storage.UserRepoI {

	if u.user == nil {
		u.user = NewUserRepo(u.db)
	}

	return u.user
}
