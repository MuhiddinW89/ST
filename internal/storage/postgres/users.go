package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"

	"sarkorT/models"
	"sarkorT/pkg/helper"
)

type userRepo struct {
	db *pgxpool.Pool
}

func NewUserRepo(db *pgxpool.Pool) *userRepo {
	return &userRepo{
		db: db,
	}
}

const (
	users_table = "users"
)

func (f *userRepo) Create(ctx context.Context, users *models.CreateUser) (string, error) {
	var (
		query   string
		user_id sql.NullString
	)

	query = fmt.Sprintf(`
		INSERT INTO %s (
			login,
			password,
			name,
			age
		) VALUES ( 
			$1,
			$2,
			$3,
			$4
		) RETURNING id
	`, users_table)

	// Exec => manage data in db but return nothing
	// QueryRow => only return one row
	// Query => return many rows

	hashed_password, err := helper.GenerateHash(users.Password)
	if err != nil {
		return "", err
	}

	users.Password = hashed_password

	row := f.db.QueryRow(
		ctx,
		query,
		helper.NewNullString(users.Login),
		helper.NewNullString(users.Password),
		helper.NewNullString(users.Name),
		users.Age,
	)

	if err := row.Scan(&user_id); err != nil {
		return "", err
	}

	return user_id.String, nil
}

func (f *userRepo) Login(ctx context.Context, req *models.LoginUser) (string, error) {
	var (
		user_id       sql.NullString
		user_login    sql.NullString
		user_password sql.NullString
	)
	query := fmt.Sprintf(`
		SELECT 
			id,
			login,
			password 
		FROM %s 
		WHERE login = $1 
	`, users_table)

	if err := f.db.QueryRow(
		ctx,
		query,
		req.Login,
	).Scan(
		&user_id,
		&user_login,
		&user_password,
	); err != nil {
		if err == sql.ErrNoRows {
			return "", errors.New("login has not been registered")
		}

		return "", err
	}

	if !helper.CheckPasswordIfMatchs(user_password.String, req.Password) {
		return "", errors.New("wrong password")
	}

	return user_id.String, nil
}

// func (f *usersRepo) GetByPKey(ctx context.Context, pkey *models.UsersPrimaryKey) (*models.Users, error) {

// 	var (
// 		id           sql.NullString
// 		first_name   sql.NullString
// 		last_name    sql.NullString
// 		login        sql.NullString
// 		password     sql.NullString
// 		phone_number sql.NullString
// 		created_at   sql.NullString
// 		updated_at   sql.NullString
// 	)

// 	query := `
// 		SELECT
// 			id,
// 			first_name,
// 			last_name,
// 			login,
// 			password,
// 			phone_number,
// 			created_at,
// 			updated_at
// 		FROM
// 			users
// 		WHERE users_id = $1
// 	`

// 	err := f.db.QueryRow(ctx, query, pkey.UsersId).
// 		Scan(
// 			&id,
// 			&first_name,
// 			&last_name,
// 			&login,
// 			&password,
// 			&phone_number,
// 			&created_at,
// 			&updated_at,
// 		)

// 	if err != nil {
// 		return nil, err
// 	}

// 	return &models.Users{
// 		Id:          id.String,
// 		FirstName:   first_name.String,
// 		LastName:    last_name.String,
// 		Login:       login.String,
// 		Password:    password.String,
// 		PhoneNumber: phone_number.String,
// 		CreatedAt:   created_at.String,
// 		UpdatedAt:   updated_at.String,
// 	}, nil
// }

// func (f *usersRepo) GetList(ctx context.Context, req *models.GetListUsersRequest) (*models.GetListUsersResponse, error) {

// 	var (
// 		resp   = models.GetListUsersResponse{}
// 		offset = " OFFSET 0"
// 		limit  = " LIMIT 5"
// 	)

// 	if req.Limit > 0 {
// 		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
// 	}

// 	if req.Offset > 0 {
// 		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
// 	}

// 	query := `
// 		SELECT
// 			COUNT(*) OVER(),
// 			id,
// 			first_name,
// 			last_name,
// 			login,
// 			password,
// 			phone_number,
// 			created_at,
// 			updated_at
// 		FROM
// 			users
// 	`

// 	query += offset + limit

// 	rows, err := f.db.Query(ctx, query)

// 	for rows.Next() {

// 		var (
// 			id           sql.NullString
// 			first_name   sql.NullString
// 			last_name    sql.NullString
// 			login        sql.NullString
// 			password     sql.NullString
// 			phone_number sql.NullString
// 			created_at   sql.NullString
// 			updated_at   sql.NullString
// 		)

// 		err := rows.Scan(
// 			&resp.Count,
// 			&id,
// 			&first_name,
// 			&last_name,
// 			&login,
// 			&password,
// 			&phone_number,
// 			&created_at,
// 			&updated_at,
// 		)

// 		if err != nil {
// 			return nil, err
// 		}

// 		resp.Userss = append(resp.Userss, &models.Users{
// 			Id:          id.String,
// 			FirstName:   first_name.String,
// 			LastName:    last_name.String,
// 			Login:       login.String,
// 			Password:    password.String,
// 			PhoneNumber: phone_number.String,
// 			CreatedAt:   created_at.String,
// 			UpdatedAt:   updated_at.String,
// 		})

// 	}

// 	return &resp, err
// }

// func (f *usersRepo) Update(ctx context.Context, req *models.UpdateUsers) (int64, error) {

// 	var (
// 		query  = ""
// 		params map[string]interface{}
// 	)

// 	query = `
// 		UPDATE
// 			users
// 		SET
// 			first_name,
// 			last_name,
// 			login,
// 			password,
// 			phone_number,
// 			updated_at = now()
// 		WHERE id = :id
// 	`

// 	params = map[string]interface{}{
// 		"id":           req.Id,
// 		"first_name":   req.FirstName,
// 		"last_name":    req.LastName,
// 		"login":        req.Login,
// 		"password":     req.Password,
// 		"phone_number": req.PhoneNumber,
// 	}

// 	query, args := helper.ReplaceQueryParams(query, params)

// 	rowsAffected, err := f.db.Exec(ctx, query, args...)
// 	if err != nil {
// 		return 0, err
// 	}

// 	return rowsAffected.RowsAffected(), nil
// }

// func (f *usersRepo) Delete(ctx context.Context, req *models.UsersPrimaryKey) error {

// 	_, err := f.db.Exec(ctx, "DELETE FROM users WHERE users_id = $1", req.UsersId)
// 	if err != nil {
// 		return err
// 	}

// 	return err
// }
