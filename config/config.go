package config

type Config struct {
	HTTPPort string

	PostgresHost           string
	PostgresUser           string
	PostgresDatabase       string
	PostgresPassword       string
	PostgresPort           string
	PostgresMaxConnections int32
}

func Load() Config {

	var cfg Config

	cfg.HTTPPort = ":4000"

	cfg.PostgresHost = "0.0.0.0"
	cfg.PostgresUser = "postgres"
	cfg.PostgresDatabase = "sarkor"
	cfg.PostgresPassword = "postgres"
	cfg.PostgresPort = "5577"
	cfg.PostgresMaxConnections = 40

	return cfg
}
