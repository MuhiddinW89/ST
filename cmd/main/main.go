package main

import (
	"context"
	"log"

	"sarkorT/api"
	"sarkorT/config"
	"sarkorT/internal/service"
	"sarkorT/internal/storage/postgres"

	"github.com/gin-gonic/gin"
)

func main() {
	// ...1 => loading configs
	cfg := config.Load()

	// ...2 => init gin
	router := gin.Default()

	// ...3 => init db
	storage, err := postgres.NewPostgres(context.Background(), cfg)
	if err != nil {
		log.Fatal(err)
	}
	defer storage.CloseDB()

	// ...4 => init service layer
	service := service.NewService(storage)

	// ...5 => init handlers
	api.SetUpApi(router, service)
	log.Printf("Listening port %v...\n", cfg.HTTPPort)

	// ...6 => start server
	var forever <-chan bool
	go router.Run(cfg.HTTPPort)
	<-forever
}
