package helper

import (
	"golang.org/x/crypto/bcrypt"
)

// var (
// 	secret_salt_for_password = "$#@$@#$#@SFDFWE324MyAmazingAndSoEasyToRememberSecretSalt#@$@#$@#$@#"
// )

// Hashing the password with the default cost of 10
func GenerateHash(password string) (string, error) {
	password_bytes := []byte(password)

	hashedPasswordBytes, err := bcrypt.GenerateFromPassword(password_bytes, bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	return string(hashedPasswordBytes), nil
}

// Comparing the password with the hash
func CheckPasswordIfMatchs(hashedPassword, currPassword string) bool {
	err := bcrypt.CompareHashAndPassword(
		[]byte(hashedPassword), []byte(currPassword))
	return err == nil
}
