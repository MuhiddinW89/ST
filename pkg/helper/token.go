package helper

import (
	"encoding/json"
	"sarkorT/models"
	"time"

	"github.com/golang-jwt/jwt"
)

var (
	secretKey   = []byte("supersecretkey")
	expiredIn   = 1 * 24 * 60 * time.Minute // expired 1 day
	payloadName = "payload"
	exp         = "exp"
)

func GenerateJWT(payloadBody string) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)

	var rb models.AccessPayload
	expiredIn = 1 * 24 * 60 * time.Minute // expired in 15 minutes
	if err := json.Unmarshal([]byte(payloadBody[:]), &rb); err != nil {
		return "", err
	}

	claims[payloadName] = payloadBody
	claims[exp] = time.Now().Add(expiredIn).Unix()
	tokenString, err := token.SignedString(secretKey)

	if err != nil {
		return "", err
	}

	return tokenString, nil
}

// func GenerateJWT(payloadBody string) (string, error) {
// 	token := jwt.New(jwt.SigningMethodHS256)
// 	claims := token.Claims.(jwt.MapClaims)

// 	claims[payloadName] = payloadBody
// 	claims[exp] = time.Now().Add(expiredIn).Unix()
// 	tokenString, err := token.SignedString(secretKey)

// 	if err != nil {
// 		return "", err
// 	}

// 	return tokenString, nil
// }
